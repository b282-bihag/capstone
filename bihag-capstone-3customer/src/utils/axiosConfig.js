export const base_url = "https://capstone-2-bihag.onrender.com/api/";
// https://capstone-2-bihag.onrender.com/api/
// http://localhost:5000/api/



const getTokenFromLocalStorage = localStorage.getItem("customer")
    ? JSON.parse(localStorage.getItem("customer"))
    : null;

export const config = {
    headers: {
        Authorization: `Bearer ${getTokenFromLocalStorage !== null ? getTokenFromLocalStorage.token : ""
            }`,
        Accept: "application/json",
    },
};