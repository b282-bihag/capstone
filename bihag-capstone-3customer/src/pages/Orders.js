import React from 'react';
import Container from '../components/Container.js';
import BreadCrumb from '../components/BreadCrumb';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { getOrders } from '../features/user/userSlice';

const Orders = () => {
    const dispatch = useDispatch();
    const orderState = useSelector((state) => state.auth.getorderedProduct?.orders);
    // console.log(orderState);

    useEffect(() => {
        dispatch(getOrders());
    }, []);

    return (
        <>
            <BreadCrumb title='My Orders' />
            <Container class1='cart-wrapper home-wrapper-2 py-5'>
                <div className='row'>
                    <div className='col-12 mt-3'>
                        <div className='row'>
                            <div className='col-3'>
                                <h5>Order Id</h5>
                            </div>
                            <div className='col-3'>
                                <h5>Total Amount</h5>
                            </div>
                            <div className='col-3'>
                                <h5>Total Amount After Discount</h5>
                            </div>
                            <div className='col-3'>
                                <h5>Status</h5>
                            </div>
                        </div>
                    </div>
                    <div className='col-12 mt-3'>
                        {
                            orderState && orderState?.map((item, index) => {
                                return (<div className='row bg- my-3' my-3 key={index}>
                                    <div className='col-3'>
                                        <p>{item?._id}</p>
                                    </div>
                                    <div className='col-3'>
                                        <p>&#8369; {item?.totalPrice}</p>
                                    </div>
                                    <div className='col-3'>
                                        <p>&#8369; {item?.totalPriceAfterDiscount}</p>
                                    </div>
                                    <div className='col-3'>
                                        <p>{item?.orderStatus}</p>
                                    </div>
                                    <div className='col-12'>
                                        <div className='row p-3 bg-secondary'>
                                            <div className='col-3'>
                                                <h6>Product Name</h6>
                                            </div>
                                            <div className='col-3'>
                                                <h6>Product Id</h6>
                                            </div>
                                            <div className='col-3'>
                                                <h6>Price</h6>
                                            </div>
                                            <div className='col-3'>
                                                <h6>Color</h6>
                                            </div>
                                            {
                                                item?.orderItems?.map((i, index) => {
                                                    return (
                                                        <div className='col-12'>
                                                            <div className='row bg-white p-3 '>
                                                                <div className='col-3'>
                                                                    <p>{i?.product?.title}</p>
                                                                </div>
                                                                <div className='col-3'>
                                                                    <p>{i?.product?._id}</p>
                                                                </div>
                                                                <div className='col-3'>
                                                                    <p>&#8369; {i?.product?.price}</p>
                                                                </div>
                                                                <div className='col-3'>
                                                                    <p>Beige</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                </div>)
                            })
                        }
                    </div>
                </div>
            </Container>
        </>
    );
};

export default Orders;
