import React from 'react'
import Meta from '../components/Meta'
import BreadCrumb from '../components/BreadCrumb'
import { MdDelete } from 'react-icons/md'
import { Link } from 'react-router-dom'
import Container from '../components/Container.js'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { deleteCartProduct, getUserCart, updateCartProduct } from '../features/user/userSlice'

const Cart = () => {

    const dispatch = useDispatch();
    const [productUpdateDetail, setproductUpdateDetail] = useState(null);
    const [totalAmount, setTotalAmount] = useState(null);
    // console.log(totalAmount);


    const userCartState = useSelector(state => state.auth.cartProducts);
    useEffect(() => {
        dispatch(getUserCart());
    }, []);
    useEffect(() => {
        if (productUpdateDetail !== null) {
            dispatch(updateCartProduct({ cartItemId: productUpdateDetail?.cartItemId, quantity: productUpdateDetail?.quantity }));
            setTimeout(() => {
                dispatch(getUserCart());
            }, 200);
        }
    }, [productUpdateDetail]);

    // console.log(userCartState);
    const deleteACartProduct = (id, quantity) => {
        dispatch(deleteCartProduct(id, quantity));
        setTimeout(() => {
            dispatch(getUserCart());
        }, 200);
    }

    useEffect(() => {
        let sum = 0;
        for (let index = 0; index < userCartState?.length; index++) {
            sum = sum + (Number(userCartState[index].quantity) * userCartState[index].price)
            // console.log(sum);
            setTotalAmount(sum)
        }
    }, [userCartState]);





    return (
        <>
            <Meta title={'Cart'} />
            <BreadCrumb title='Cart' />
            <Container class1='cart-wrapper home-wrapper-2 py-5'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='cart-header py-3 d-flex justify-content-between align-items-center '>
                            <h4 className='cart-col-1'>Product</h4>
                            <h4 className='cart-col-2'>Price</h4>
                            <h4 className='cart-col-3'>Quantity</h4>
                            <h4 className='cart-col-4'>Total</h4>
                        </div>
                        {
                            userCartState && userCartState?.map((item, index) => {
                                return (<div className='cart-data py-3 mb-2 d-flex justify-content-between align-items-center '>
                                    <div className='cart-col-1 gap-15 d-flex align-items-center'>
                                        <div className='w-25'>
                                            <img src={item?.productId?.images[0]?.url} className='img-fluid' alt='product' />
                                        </div>
                                        <div className='w-75'>
                                            <h5 className='title'>{item?.productId.title}</h5>
                                            <p className='color'>{item?.color?.title}</p>

                                        </div>
                                    </div>
                                    <div className='cart-col-2'>
                                        <h5 className='price'>&#8369; {item?.productId.price}</h5>
                                    </div>
                                    <div className='cart-col-3 d-flex align-items-center gap-15'>
                                        <div><input type='number'
                                            style={{ width: "70px" }}
                                            name=''
                                            placeholder='1'
                                            min={1}
                                            max={10}
                                            className='form-control'
                                            id=''
                                            value={productUpdateDetail?.quantity ? productUpdateDetail?.quantity : item?.quantity}
                                            onChange={(e) => { setproductUpdateDetail({ cartItemId: item?._id, quantity: e.target.value }) }}
                                        />
                                        </div>
                                        <div className='fs-4'>
                                            <MdDelete onClick={() => { deleteACartProduct(item?._id) }} className='text-danger' />
                                        </div>
                                    </div>
                                    <div className='cart-col-4'><h5 className='price'>&#8369; {item?.price * item?.quantity}</h5>
                                    </div>
                                </div>)
                            })
                        }

                    </div>
                    <div>
                        <div className='col-12 py-2 mt-4'>
                            <div className='d-flex justify-content-between align-items-baseline'>
                                <Link to='/shop' className='button'>Continue Shopping</Link>
                                {
                                    (totalAmount !== null || totalAmount !== 0) && <div className='d-flex flex-column align-items-end'>
                                        <h4>SubTotal: &#8369; {totalAmount}</h4>
                                        <p>Taxes and shipping fees are calculated during checkout</p>
                                        <Link to='/checkout' className='button'>Check Out</Link>
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </>
    )
}

export default Cart