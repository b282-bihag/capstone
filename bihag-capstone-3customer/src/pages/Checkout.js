import React from 'react'
import { Link, Navigate } from 'react-router-dom'
import { IoIosArrowBack } from 'react-icons/io'
import Container from '../components/Container.js'
import { useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { useFormik } from 'formik'
import * as yup from 'yup'

// import axios from 'axios'
// import { config } from '../utils/axiosConfig'


const shippingSchema = yup.object({
    firstName: yup.string().required("First Name is required"),
    lastName: yup.string().required("Last Name is required"),
    address: yup.string().required("Address is required"),
    state: yup.string().required("State is required"),
    city: yup.string().required("City is required"),
    country: yup.string().required("Country is required"),
    pincode: yup.number().required("Pincode is required"),
});


const Checkout = () => {
    // const dispatch = useDispatch()
    const cartState = useSelector(state => state.auth.cartProducts)
    const [totalAmount, setTotalAmount] = useState(null);
    // console.log(cartState)
    // const [shippingInfo, setShippingInfo] = useState(null);

    useEffect(() => {
        let sum = 0;
        for (let index = 0; index < cartState?.length; index++) {
            sum = sum + (Number(cartState[index].quantity) * cartState[index].price)
            // console.log(sum);
            setTotalAmount(sum)
        }
    }, [cartState])

    const formik = useFormik({
        initialValues: {
            firstName: "",
            lastName: "",
            address: "",
            state: "",
            city: "",
            country: "",
            pincode: "",
            other: "",
        },
        validationSchema: shippingSchema,
        onSubmit: values => {

            // setShippingInfo(values)
            Navigate('/my-orders')
            // checkOutHandler()
        },
    });


    // const loadScript = (src) => {
    //     return new Promise((resolve) => {
    //         const script = document.createElement("script");
    //         script.src = src;
    //         script.onload = () => {
    //             resolve(true);
    //         }
    //         script.onerror = () => {
    //             resolve(false);
    //         }
    //         document.body.appendChild(script)
    //     })
    // }

    // const checkOutHandler = async () => {
    //     const res = await loadScript("https://checkout.razorpay.com/v1/checkout.js")
    //     if (!res) {
    //         alert("Razorpay Failed To Load")
    //         return;
    //     }
    //     const result = await axios.post("http://localhost:5000/api/user/order/checkout", "", config)
    //     if (!result) {
    //         alert("Something Went Wrong")
    //         return
    //     }
    //     const { amount, id: order_id, currency } = result.data
    //     const options = {
    //         key: "rzp_test_jk96M1tbCBGW2H", // Enter the Key ID generated from the Dashboard
    //         amount: amount.toString(),
    //         currency: currency,
    //         name: "Ara Ukay Alaminos",
    //         description: "Test Transaction",
    //         // image: { logo },
    //         order_id: order_id,
    //         handler: async function (response) {
    //             const data = {
    //                 orderCreationId: order_id,
    //                 razorpayPaymentId: response.razorpay_payment_id,
    //                 razorpayOrderId: response.razorpay_order_id,
    //                 razorpaySignature: response.razorpay_signature,
    //             };

    //             const result = await axios.post("http://localhost:5000/api/user/order/paymentVerification", data);

    //             alert(result);
    //         },
    //         prefill: {
    //             name: "Rynn Bihag",
    //             email: "ceakee@gmail.com",
    //             contact: "09123456789",
    //         },
    //         notes: {
    //             address: "Agno, Pangasinan",
    //         },
    //         theme: {
    //             color: "#61dafb",
    //         },
    //     };

    //     const paymentObject = new window.Razorpay(options);
    //     paymentObject.open();
    // }


    return (
        <>
            <Container class1='checkout-wrapper py-5 home-wrapper-2'>
                <div className='row'>
                    <div className='col-7'>
                        <div className='checkout-left-data'>
                            <h3 className='website-name'>Ara Ukay Alaminos</h3>
                            <nav style={{ "--bs-breadcrumb-divider": "'>'" }} aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><Link className='text-dark' total-price to='/cart'>Cart</Link></li>
                                    <li className="breadcrumb-item total-price active" aria-current="page">Information</li>
                                    <li className="breadcrumb-item total-price active">Shipping</li>
                                    <li className="breadcrumb-item total-price active" aria-current="page">Payment</li>
                                </ol>
                            </nav>
                            <h4 className='title total'>
                                Contact Information
                            </h4>
                            <p className='user-details total'>Rynn Bihag (ceakee@gmail.com)</p>
                            <h4 className='mb-3'>Shipping Address</h4>
                            <form onSubmit={formik.handleSubmit}
                                action='' className='d-flex  flex-wrap gap-15 justify-content-between'>
                                <div className='w-100'>
                                    <select
                                        name='country'
                                        value={formik.values.country}
                                        onChange={formik.handleChange('country')}
                                        onBlur={formik.handleBlur('country')}
                                        className='form-control form-select' id=''
                                    >
                                        <option value=''>Select Country
                                        </option>
                                        <option value='Philippines'>Philippines
                                        </option>
                                    </select>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.country && formik.errors.country
                                        }
                                    </div>
                                </div>
                                <div className='flex-grow-1'>
                                    <input type='text' placeholder='First Name' className='form-control'
                                        name='firstName'
                                        value={formik.values.firstName}
                                        onChange={formik.handleChange('firstName')}
                                        onBlur={formik.handleBlur('firstName')}></input>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.firstName && formik.errors.firstName
                                        }
                                    </div>
                                </div>
                                <div className='flex-grow-1'>
                                    <input type='text' placeholder='Last Name' className='form-control'
                                        name='lastName'
                                        value={formik.values.lastName}
                                        onChange={formik.handleChange('lastName')}
                                        onBlur={formik.handleBlur('lastName')}></input>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.lastName && formik.errors.lastName
                                        }
                                    </div>
                                </div>
                                <div className='w-100'>
                                    <input type='text' placeholder='Street Name, Building, House No.' className='form-control'
                                        name='other'
                                        value={formik.values.other}
                                        onChange={formik.handleChange('other')}
                                        onBlur={formik.handleBlur('other')}>
                                    </input>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.other && formik.errors.other
                                        }
                                    </div>
                                </div>
                                <div className='w-100'>
                                    <input type='text'
                                        placeholder='Barangay, Province' className='form-control'
                                        name='address'
                                        value={formik.values.address}
                                        onChange={formik.handleChange('address')}
                                        onBlur={formik.handleBlur('address')}
                                    ></input>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.address && formik.errors.address
                                        }
                                    </div>
                                </div>
                                <div className='flex-grow-1'>
                                    <input type='text' placeholder='City' className='form-control'
                                        name='city'
                                        value={formik.values.city}
                                        onChange={formik.handleChange('city')}
                                        onBlur={formik.handleBlur('city')}
                                    ></input>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.city && formik.errors.city
                                        }
                                    </div>
                                </div>
                                <div className='flex-grow-1'>
                                    <select
                                        name='state'
                                        value={formik.values.state}
                                        onChange={formik.handleChange('state')}
                                        onBlur={formik.handleBlur('state')} className='form-control form-select' id='' >
                                        <option value=''>Select Region</option>
                                        <option value='Luzon'>Luzon</option>
                                    </select>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.state && formik.errors.state
                                        }
                                    </div>
                                </div>
                                <div className='flex-grow-1'>
                                    <input type='text' placeholder='Postal Code' className='form-control'
                                        name='pincode'
                                        value={formik.values.pincode}
                                        onChange={formik.handleChange('pincode')}
                                        onBlur={formik.handleBlur('pincode')}
                                    ></input>
                                    <div className='error ms-2 my-1' >
                                        {
                                            formik.touched.pincode && formik.errors.pincode
                                        }
                                    </div>
                                </div>
                                <div className='w-100'>
                                    <div className='d-flex justify-content-between align-items-center'>
                                        <Link to='/cart' className='text-dark' > <IoIosArrowBack className='me-2' /> Return to Cart</Link>
                                        <Link to='/my-orders' className='button' >Continue to Shipping</Link>
                                        <button to='/my-orders' className='button' type=''>
                                            Place Order
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className='col-5'>
                        <div className='border-bottom py-4'>
                            {
                                cartState && cartState?.map((item, index) => {
                                    return (<div key={index} className='d-flex gap-10 align-items-center'>
                                        <div className='w-75 d-flex mb-2 gap-10'>
                                            <div className='w-25 position-relative'>
                                                <span style={{ top: "-10px", right: "-2px" }} className='badge bg-secondary rounded-circle p-2 position-absolute'>
                                                    {item?.quantity}
                                                </span>
                                                <img className='img-fluid' src={item?.productId?.images[0]?.url} alt='product' />
                                            </div>
                                            <div>
                                                <h5 className='total'>{item?.productId?.title}</h5>
                                                <p className='total-price'>{item?.color?.title}</p>
                                            </div>
                                        </div>
                                        <div className='flex-grow-1'>
                                            <h5 className='total'>&#8369; {item?.price * item?.quantity}</h5>
                                        </div>
                                    </div>)
                                })
                            }

                        </div>
                        <div className='border-bottom py-4'>
                            <div className='d-flex justify-content-between align-items-center'>
                                <p className='total'>Subtotal</p>
                                <p className='total-price'>&#8369; {totalAmount ? totalAmount : "0"}</p>
                            </div>
                            <div className='d-flex justify-content-between align-items-center'>
                                <p className='mb-0 total'>Shipping</p>
                                <p className='mb-0 total-price'>&#8369; 100</p>
                            </div>
                        </div>
                        <div className='d-flex justify-content-between align-items-center py-4'>
                            <h4 className='total'>Total</h4>
                            <h5 className='total-price'>&#8369; {totalAmount ? totalAmount + 100 : "0"}</h5>
                        </div>
                    </div>
                </div>
            </Container>
        </>
    )
}

export default Checkout