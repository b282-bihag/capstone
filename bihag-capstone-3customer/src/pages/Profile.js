import React, { useState } from 'react'
import Container from '../components/Container.js'
import BreadCrumb from '../components/BreadCrumb.js'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { updateProfile } from '../features/user/userSlice.js'
import { FiEdit } from 'react-icons/fi'




const profileSchema = yup.object({
    firstName: yup.string().required("First Name is required"),
    lastName: yup.string().required("Last Name is required"),
    email: yup.string().required("Email is required").email("Must be a valid email"),
    mobile: yup.number().required("Mobile Number is required"),
});

const Profile = () => {

    const dispatch = useDispatch();
    const userState = useSelector(state => state.auth.user)
    const [edit, setEdit] = useState(true)

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            firstName: userState?.firstName,
            lastName: userState?.lastName,
            email: userState?.email,
            mobile: userState?.mobile,
        },
        validationSchema: profileSchema,
        onSubmit: values => {
            dispatch(updateProfile(values))
            setEdit(true)

        },
    });


    return (
        <>
            <BreadCrumb title='My Profile' />
            <Container class1='cart-wrapper home-wrapper-2 py-5'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='d-flex justify-content-between align-items-center'>
                            <h3 className='my-3'>Update User Profile</h3>
                            <FiEdit className='fs-3' onClick={() => setEdit(false)} />
                        </div>
                    </div>
                    <div className='col-12'>
                        <form onSubmit={formik.handleSubmit}>


                            <div className="mb-3">
                                <label htmlFor="example1" className="form-label">First Name</label>
                                <input type="text" name='firstName' disabled={edit} className="form-control" id="example1"
                                    value={formik.values.firstName}
                                    onChange={formik.handleChange('firstName')}
                                    onBlur={formik.handleBlur('firstName')}
                                />
                                <div className='error'>{formik.touched.firstName && formik.errors.firstName}</div>
                            </div>

                            <div className="mb-3">
                                <label htmlFor="example2" className="form-label">Last Name</label>
                                <input type="text" name='lastName' className="form-control" disabled={edit} id="example2"
                                    value={formik.values.lastName}
                                    onChange={formik.handleChange('lastName')}
                                    onBlur={formik.handleBlur('lastName')} />
                                <div className='error'>{formik.touched.lastName && formik.errors.lastName}</div>
                            </div>

                            <div className="mb-3">
                                <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                                <input type="email" name='email' className="form-control" disabled={edit} id="exampleInputEmail1" aria-describedby="emailHelp"
                                    value={formik.values.email}
                                    onChange={formik.handleChange('email')}
                                    onBlur={formik.handleBlur('email')}
                                />
                                <div className='error'>{formik.touched.email && formik.errors.email}</div>

                            </div>

                            <div className="mb-3">
                                <label htmlFor="exampleInputEmail2" className="form-label">Mobile Number</label>
                                <input type="number" name='mobile' disabled={edit} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                    value={formik.values.mobile}
                                    onChange={formik.handleChange('mobile')}
                                    onBlur={formik.handleBlur('mobile')}
                                />
                                <div className='error'>{formik.touched.mobile && formik.errors.mobile}</div>
                            </div>

                            {/* <div className="mb-3 form-check">
                                <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                                <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                            </div> */}
                            {
                                edit === false && <button type="submit" className="btn btn-primary">Save</button>
                            }
                        </form>
                    </div>
                </div>
            </Container>
        </>
    )
}

export default Profile