import React from 'react'
import Meta from '../components/Meta'
import BreadCrumb from '../components/BreadCrumb'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react';
import { getUserProductWishlist } from '../features/user/userSlice.js';
import Container from '../components/Container.js';


function Wishlist() {

    const dispatch = useDispatch();
    useEffect(() => {
        getWishlistFromDb();
    }, []);
    const getWishlistFromDb = () => {
        dispatch(getUserProductWishlist());
    };
    const wishlistState = useSelector((state) => state.auth.wishlist.wishlist);
    const removeFromWishlist = (id) => {
        dispatch(getUserProductWishlist());
        setTimeout(() => {
            dispatch(getUserProductWishlist());
        }, 300);
    };

    return (
        <>
            <Meta title={'Wishlist'} />
            <BreadCrumb title='Wishlist' />
            <Container className="wishlist-wrapper home-wrapper-2 py-5">

                <div className='row'>
                    {
                        wishlistState?.map((item, index) => {
                            return (
                                <div className='col-3' key={index}>
                                    <div className='wishlist-card position-relative'>
                                        <img onClick={() => { removeFromWishlist(item?._id) }}
                                            src='images/cross.svg'
                                            alt='cross'
                                            className='position-absolute cross img-fluid'
                                        />
                                        <div className='wishlist-card-image bg-white'>
                                            <img
                                                src={item?.images[0].url ? item?.images[0].url : 'images/sprod1.png'}
                                                className='img-fluid d-block mx-auto' alt='dress'
                                                width={200}
                                            />
                                        </div>
                                        <div className='py-3 px-3'>
                                            <h5 className='title'>{item?.title}</h5>
                                            <h6 className='price'>&#8369; {item?.price}</h6>
                                        </div>
                                    </div>
                                </div>

                            )
                        })
                    }

                </div>
            </Container>
        </>
    );
};

export default Wishlist