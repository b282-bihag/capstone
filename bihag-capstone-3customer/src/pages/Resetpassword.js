import React from 'react'
import Meta from '../components/Meta'
import BreadCrumb from '../components/BreadCrumb'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import Container from '../components/Container.js'
import CustomInput from '../components/CustomInput.js'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import { resetPassword } from '../features/user/userSlice.js';



const passwordSchema = yup.object({
    password: yup.string().required("New Password is required"),
    // password: yup.string().required("Confirm Password is required"),
});


const Resetpassword = () => {

    const location = useLocation()
    const getToken = location.pathname.split('/')[2]
    console.log(getToken);

    const dispatch = useDispatch();
    const navigate = useNavigate();
    const formik = useFormik({
        initialValues: {
            password: '',
        },
        validationSchema: passwordSchema,
        onSubmit: values => {
            dispatch(resetPassword({ token: getToken, password: values.password }));
            navigate('/login')
        },
    });



    return (
        <>
            <Meta title={'Reset Password'} />
            <BreadCrumb title='Reset Password' />
            <Container class1='login-wrapper py-5 home-wrapper-2'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='auth-card'>
                            <h3 className='text-center mb-3'>Reset Password</h3>
                            <form action='' onSubmit={formik.handleSubmit} className='d-flex flex-column gap-30'>
                                <CustomInput
                                    type='password'
                                    name='password'
                                    placeholder='Type New Password'
                                    onChange={formik.handleChange('password')}
                                    onBlur={formik.handleBlur('password')}
                                    value={formik.values.password}
                                />
                                <div className='error'>
                                    {formik.touched.password && formik.errors.password}
                                </div>
                                {/* <CustomInput
                                    type='password'
                                    name='confpassword'
                                    placeholder='Confirm New Password'
                                    required
                                /> */}
                                <div>
                                    <div className='mt-3 d-flex justify-content-center flex-column gap-15 align-items-center'>
                                        <button className='button border-0' type='submit'>Submit</button>
                                        <Link to='/login'>Cancel</Link>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Container>
        </>
    )
}

export default Resetpassword