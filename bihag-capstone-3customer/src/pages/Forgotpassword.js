import React from 'react'
import Meta from '../components/Meta'
import BreadCrumb from '../components/BreadCrumb'
import { Link } from 'react-router-dom'
import Container from '../components/Container.js'
import CustomInput from '../components/CustomInput.js'
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useDispatch } from 'react-redux';
import { forgotPasswordToken } from '../features/user/userSlice.js'



const emailSchema = yup.object({
    email: yup.string().required("Email is required").email("Must be a valid email"),
});


const Forgotpassword = () => {


    const dispatch = useDispatch();
    // const navigate = useNavigate();
    const formik = useFormik({
        initialValues: {
            email: '',
        },
        validationSchema: emailSchema,
        onSubmit: values => {
            dispatch(forgotPasswordToken(values));
        },
    });



    return (
        <>
            <Meta title={'Forgot Password'} />
            <BreadCrumb title='Forgot Password' />
            <Container class1='login-wrapper py-5 home-wrapper-2'>
                <div className='row'>
                    <div className='col-12'>
                        <div className='auth-card'>
                            <h3 className='text-center mb-3'>Forgot Password</h3>
                            <p className='text-center mt-2 mb-3'>Enter your Email, we'll send you a link to reset your password.</p>
                            <form action='' onSubmit={formik.handleSubmit} className='d-flex flex-column gap-30'>
                                <CustomInput
                                    className='mt-3'
                                    type='email'
                                    name='email'
                                    placeholder='Email'
                                    onChange={formik.handleChange('email')}
                                    onBlur={formik.handleBlur('email')}
                                    value={formik.values.email}
                                />
                                <div className='error text-center'>
                                    {formik.touched.email && formik.errors.email}
                                </div>
                                <div>
                                    <div className='mt-2 d-flex justify-content-center flex-column gap-15 align-items-center'>
                                        <button className='button border-0' type='submit'>Submit</button>
                                        <Link to='/login'>Cancel</Link>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Container>
        </>
    )
}

export default Forgotpassword