import axios from "axios";
import { base_url } from "../../utils/axiosConfig";
import axiosInstance from "../../utils/axiosProdConfig";

const getProducts = async () => {
    try {
        const response = await axiosInstance.get("product");
        if (response.data) {
            return response.data;
        }
    } catch (error) {
        throw error;
    }
};

const addToWishlist = async (prodId) => {
    const response = await axios.put(`${base_url}product/wishlist`, { prodId });
    if (response.data) {
        return response.data;
    }
};


export const productService = {
    getProducts,
    addToWishlist,
};
