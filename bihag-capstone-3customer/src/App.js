import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';
import Layout from './components/Layout';
import About from './pages/About';
import Contact from './pages/Contact';
import Home from './pages/Home';
import ViewShop from './pages/ViewShop';
import CompareProduct from './pages/CompareProduct.js';
import Wishlist from './pages/Wishlist.js';
import Login from './pages/Login.js';
import Forgotpassword from './pages/Forgotpassword.js';
import Signup from './pages/Signup.js';
import Resetpassword from './pages/Resetpassword.js';
import PrivacyPolicy from './pages/PrivacyPolicy.js';
import RefundPolicy from './pages/RefundPolicy.js';
import ShippingPolicy from './pages/ShippingPolicy.js';
import TermsAndConditions from './pages/TermsAndConditions.js';
import SingleProduct from './pages/SingleProduct.js';
import Cart from './pages/Cart.js';
import Checkout from './pages/Checkout.js';
import { PrivateRoutes } from './routing/PrivateRoutes.js';
import { OpenRoutes } from './routing/OpenRoutes.js';
import Orders from './pages/Orders.js';
import Profile from './pages/Profile.js';

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="about" element={<About />} />
            <Route path="contact" element={<PrivateRoutes><Contact /></PrivateRoutes>} />
            <Route path="shop" element={<PrivateRoutes><ViewShop /></PrivateRoutes>} />
            <Route path="product" element={<PrivateRoutes><SingleProduct /></PrivateRoutes>} />
            <Route path="cart" element={<PrivateRoutes><Cart /></PrivateRoutes>} />
            <Route path="my-orders" element={<PrivateRoutes><Orders /></PrivateRoutes>} />
            <Route path="my-profile" element={<PrivateRoutes><Profile /></PrivateRoutes>} />
            <Route path="checkout" element={<PrivateRoutes><Checkout /></PrivateRoutes>} />
            <Route path="compare-product" element={<PrivateRoutes><CompareProduct /></PrivateRoutes>} />
            <Route path="wishlist" element={<PrivateRoutes><Wishlist /></PrivateRoutes>} />
            <Route path="login" element={<OpenRoutes><Login /></OpenRoutes>} />
            <Route path="forgot-password" element={<Forgotpassword />} />
            <Route path="sign-up" element={<OpenRoutes><Signup /></OpenRoutes>} />
            <Route path="reset-password/:token" element={<Resetpassword />} />
            <Route path="privacy-policy" element={<PrivacyPolicy />} />
            <Route path="refund-policy" element={<RefundPolicy />} />
            <Route path="shipping-policy" element={<ShippingPolicy />} />
            <Route path="terms-conditions" element={<TermsAndConditions />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
