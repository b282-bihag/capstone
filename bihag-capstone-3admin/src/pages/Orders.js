import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getOrders } from "../features/auth/authSlice";
const columns = [
    {
        title: "SNo",
        dataIndex: "key",
    },
    {
        title: "Name",
        dataIndex: "name",
    },
    {
        title: "Product",
        dataIndex: "product",
    },
    {
        title: "Amount",
        dataIndex: "amount",
    },
    {
        title: "Date",
        dataIndex: "date",
    },
];

const Orders = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getOrders());
    }, []);

    const orderState = useSelector((state) => state.auth.orders[0]);

    const data1 = [];
    if (orderState) {
        data1.push({
            key: 1,
            name: `${orderState.orderby.firstName} ${orderState.orderby.lastName}`,
            product: (
                <Link to={`/admin/order/${orderState.orderby._id}`}>
                    View Orders
                </Link>
            ),
            amount: orderState.paymentIntent.amount,
            date: new Date(orderState.createdAt).toLocaleString(),
        });
    }



    return (
        <div>
            <h3 className="mb-4 title">Orders</h3>
            <div>{<Table columns={columns} dataSource={data1} />}</div>
        </div>
    );
};

export default Orders;